from setuptools import setup

#
###
setup(
  name="tcpclientserver",
  version="1.0.0",
  description="Python3 module to open tcp-socket server for remote requests",
  keywords="tcp socket remote requests",
  # Project's main page 
  #url="https://gitlab.com/mvimplis2013/tcp-client-server",
  url="https://gitlab.com/robert.berger/python3-tcpclientserver",
  # Author details
  author="Miltos K. Vimplis", 
  author_email="mvimblis@gmail.com",
  # Choose your license
  license="MIT",
  classifiers=[
    "Programing Language :: Python :: 3.7",
    "Operating System :: Linux", 
    "Environment :: Console"
  ],
  packages=["tcpclientserver"],
  install_requires=["configparser"], 
# it's a module/library, so no entry point
#  entry_points={
#    "console_scripts": [
#      "tcpclientserver=tcpclientserver.server:main",
#     ],
#  },
  zip_safe=True
)
